FROM 'nginx'
COPY ./ /var/www/html
RUN rm /etc/nginx/nginx.conf && echo "events {  worker_connections  1024;  }  http { server { listen 80; root /var/www/html; } }" >> /etc/nginx/nginx.conf